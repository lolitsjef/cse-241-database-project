public class currentCustomer {
    private static int SSN;
    private static String name;
    private static String email;
    private static String phoneNum;
    private static int house_id;
    private static int house_num;
    private static String street_name;
    private static String city;
    private static String state;
    private static int zip;


    public static String getState() {
        return state;
    }
    public static void setState(String k) {
        state = k;
    }
    public static String getCity() {
        return city;
    }
    public static void setCity(String k) {
        city = k;
    }
    public static String getStreetname() {
        return street_name;
    }
    public static void setStreetName(String k) {
        street_name = k;
    }
    public static int getHouseNum() {
        return house_num;
    }

    public static void setHouseNum(int k) {
        house_num = k;
    }
    public static int getHouseId() {
        return house_id;
    }
    public static void setHouseId(int k) {
        house_id = k;
    }
    public static int getZip() {
        return zip;
    }

    public static void setZip(int k) {
        zip = k;
    }
    public static int getSSN() {
        return SSN;
    }
    public static void setSSN(int k) {
        SSN = k;
    }

    public static String getPhoneNum() {
        return phoneNum;
    }
    public static void setPhoneNum(String k) {
        phoneNum = k;
    }
    
    public static String getName() {
        return name;
    }
    public static void setName(String k) {
        name = k;
    }

    public static String getEmail() {
        return email;
    }
    public static void setEmail(String k) {
        email = k;
    }
}