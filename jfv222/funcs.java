import java.io.*;
import java.sql.*;
import java.util.*;


public class funcs {

    /* clears the screen and prints the header */
    public static void clear() {
		System.out.print(colors.ANSI_CLEAR); //Clears terminal
		System.out.flush();
        System.out.println(colors.TEXT_BRIGHT_YELLOW + colors.ANSI_BOLD +  colors. ANSI_UNDER + 
                           "        Nickel Savings and Loan        \n" + colors.ANSI_RESET + colors.TEXT_WHITE);        
    }
    /* prints the welcome screen */
    public static void welcome() {
        
        clear();
        System.out.println(colors.ANSI_BOLD + "  Welcome to Nickel Savings and Loan\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Connect\n" +
                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " Help\n" +
                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Quit\n");

    }
    /* reads a positive integer */
    public static int readInt () {
        Scanner stdin = new Scanner(System.in);
            if (stdin.hasNextInt()){
                int k = stdin.nextInt();
                if (k > 0) return k; }
        return -1;
    }
    /* reads a positive double */
    public static double readDouble () {
        Scanner stdin = new Scanner(System.in);
            if (stdin.hasNextDouble()){
                double k = stdin.nextDouble();
                if (k > 0) return k; }
        return -1;
    }
    /* prints the help message from the welcome screen */
    public static void help() {
        clear();
        System.out.println(colors.ANSI_BOLD + "Help:\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
        System.out.println("  Welcome to the online banking system");
        System.out.println("  for Nickel Savings and Loan. To run  ");
        System.out.println("  this program first login and connect ");
        System.out.println("  to the database. Login using your 9 digit");
        System.out.println("  social scurity number. Then use the number ");
        System.out.println("  keys to select the action you want to");
        System.out.println("  perform. More info in README.                  Press enter to continue.     ");
    }
    /* connects to the database using the users information, taken from homework 4, returns the connection */
    public static Connection connect() {
        Scanner stdin = new Scanner(System.in);
        String username;
        String password;
        Connection con = null;
        Boolean connected = false;
        do {
            /* Get login information */
            clear();
            System.out.println(colors.ANSI_BOLD + "Connect:\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
            System.out.print(colors.ANSI_BOLD + "Username: " + colors.ANSI_RESET + colors.TEXT_WHITE);                                                           
            username = stdin.nextLine();
            System.out.print(colors.ANSI_BOLD + "Password: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            password = stdin.nextLine();

            clear();
            System.out.println("Connecting to Database...");
            
            try {
                /* Establish connection using login information  THE LEHIGH DATABASE IS HARDCODED IN, 
                                                                 TO RUN THIS ON YOUR OWN DATABASE THE LINK NEEDS TO BE CHANGED */
                con = DriverManager.getConnection                                               
                    ("jdbc:oracle:thin:@edgar1.cse.lehigh.edu:1521:cse241", username, password);
                connected = true;
            } catch (Exception badLogin) {
                System.out.println("Invalid username or password");
            }

        } while (!connected);
        return con;
    }
    /* prints the main menu screen */
    public static void mainMenu() {
        clear();

        System.out.println(colors.ANSI_BOLD + "Currently Logged in as: " + colors.ANSI_RESET + colors.TEXT_WHITE + currentCustomer.getName() + "\n");
        System.out.println(colors.NUMBER + "  1: " + colors.ANSI_RESET + " View Your Accounts, Cards, and Loans");
        System.out.println(colors.NUMBER + "  2: " + colors.ANSI_RESET + " Open New Account");
        System.out.println(colors.NUMBER + "  3: " + colors.ANSI_RESET + " Deposit");
        System.out.println(colors.NUMBER + "  4: " + colors.ANSI_RESET + " Withdraw");
        System.out.println(colors.NUMBER + "  5: " + colors.ANSI_RESET + " Purchase Using Credit Card");
        System.out.println(colors.NUMBER + "  6: " + colors.ANSI_RESET + " Purchase Using Debit Card");
        System.out.println(colors.NUMBER + "  7: " + colors.ANSI_RESET + " Help");
        System.out.println(colors.NUMBER + "  8: " + colors.ANSI_RESET + " Logout");
        System.out.println(colors.NUMBER + "  9: " + colors.ANSI_RESET + " Quit");
    }
    /* has the user login with their ssn, updates the information of currentCustomer based on the users ssn */
    public static void login(Connection con) throws SQLException, IOException {
        clear();
        Scanner stdin = new Scanner(System.in);
        Statement s = con.createStatement();
        String query;
        int userSSN;
        ResultSet result;
        Boolean hasOutputs = false;

        
        do {
            System.out.print(colors.ANSI_BOLD + "Enter SSN: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            userSSN = readInt();
            query = "SELECT * FROM customer WHERE SSN = " + userSSN;

            result = s.executeQuery(query);

            if (result.next()) {
                hasOutputs = true;
            }
            else {
                clear();
                System.out.println(colors.TEXT_RED + "Sorry, we don't have that SSN on file, please try again" + colors.TEXT_WHITE);
            }
        } while (!hasOutputs);
        currentCustomer.setName(result.getString("name"));
        currentCustomer.setEmail(result.getString("email"));
        currentCustomer.setSSN(result.getInt("SSN"));
        currentCustomer.setPhoneNum(result.getString("phone_num"));
        getAddress(con);   
    }
    /* prints all of the users information including their accounts, cards, and loans */
    public static void viewInfo(Connection con) throws SQLException, IOException {
        clear ();
        printPersonal();

        Scanner stdin = new Scanner(System.in);
        Statement s = con.createStatement();
        String query;
        ResultSet result;
        boolean hasResults = false;


            /* SAVINGS ACCOUNTS */
            hasResults = false;
            query = 
                "SELECT account.account_num, account.account_type, savings_account.balance, savings_account.min_balance, savings_account.interest_rate " +
                "FROM " +
                "customer join customers_on_account ON customer.SSN = customers_on_account.SSN " +
                "join account ON account.account_num = customers_on_account.account_num " +
                "join savings_account ON account.account_num = savings_account.account_num " +
                "WHERE customer.ssn = " + currentCustomer.getSSN() + 
                " and account.account_type = 'savings'";
            result = s.executeQuery(query);
    
                
                System.out.println(colors.NUMBER + colors.ANSI_UNDER + "Savings Accounts" + colors.ANSI_RESET);
                System.out.print(colors.ANSI_BOLD  + colors.ANSI_UNDER);
                System.out.printf("%20s %20s %20s %20s %20s\n", "Account Number", "Account Type", "Balance", "Minimum Balance", "Interest Rate");
                System.out.println(colors.ANSI_RESET);
                    while (result.next()) {
                        hasResults = true;
                        System.out.printf("%20s %20s %20s %20s %20s\n", String.format("%05d", result.getInt("account_num")),
                                           result.getString("account_type"), "$" + result.getString("balance"), "$" + result.getString("min_balance"),
                                           result.getString("interest_rate") + "%");
                    }
                if (!hasResults)
                System.out.println("               No savings accounts on file\n");

             /* CHECKING ACCOUNTS */
             query = 
             "SELECT account.account_num, account.account_type, checking_account.balance, checking_account.interest_rate " +
             "FROM " +
             "customer join customers_on_account ON customer.SSN = customers_on_account.SSN " +
             "join account ON account.account_num = customers_on_account.account_num " +
             "join checking_account ON account.account_num = checking_account.account_num " +
             "WHERE customer.ssn = " + currentCustomer.getSSN() + 
             " and account.account_type = 'checking'";
            result = s.executeQuery(query);
 
             
             System.out.println(colors.NUMBER + colors.ANSI_UNDER + "Checking Accounts" + colors.ANSI_RESET);
             System.out.print(colors.ANSI_BOLD  + colors.ANSI_UNDER);
             System.out.printf("%20s %20s %20s %20s %20s \n", "Account Number", "Account Type", "Balance", "", "Interest Rate");
             System.out.println(colors.ANSI_RESET);
                 while (result.next()) {
                     hasResults = true;
                     System.out.printf("%20s %20s %20s %20s %20s\n", String.format("%05d", result.getInt("account_num")),
                                        result.getString("account_type"), "$" + result.getString("balance"), "", 
                                        result.getString("interest_rate") + "%");
                 }
             if (!hasResults)
             System.out.println("               No checking accounts on file\n");
                
            /* DEBIT CARDS */
            hasResults = false;
            query = 
                "SELECT debit_card.card_num, debit_card.account_num " +
                "FROM debit_card join customer " +
                "ON debit_card.ssn = customer.ssn " +
                "WHERE customer.ssn = " + currentCustomer.getSSN(); 
            result = s.executeQuery(query);
    
                System.out.println(colors.NUMBER + colors.ANSI_UNDER + "Debit Cards" + colors.ANSI_RESET);
                System.out.print(colors.ANSI_BOLD  + colors.ANSI_UNDER);
                System.out.printf("%20s %20s \n", "Acount Number", "Card Number");
                System.out.println(colors.ANSI_RESET);
                    while (result.next()) {
                        hasResults = true;
                        
                        System.out.printf("%20s %20s\n", String.format("%05d", result.getInt("account_num")), String.format("%05d", result.getInt("card_num")));
                                           
                    }
                if (!hasResults)
                System.out.println("               No debit cards on file");

            /* Credit CARDS */
            hasResults = false;
            query = 
            "SELECT credit_card.card_num, credit_card.interest_rate, credit_card.credit_limit, " + 
            "credit_card.running_bal, credit_card.due_bal " + 
            "FROM credit_card join customer " + 
            "ON credit_card.ssn = customer.ssn " + 
            "where customer.ssn = " + currentCustomer.getSSN();

            result = s.executeQuery(query);
    
                System.out.println(colors.NUMBER + colors.ANSI_UNDER + "Credit Cards" + colors.ANSI_RESET);
                System.out.print(colors.ANSI_BOLD  + colors.ANSI_UNDER);
                System.out.printf("%20s %20s %20s %20s %20s\n", "Card Number", "Due Balance", "Running Balance ", "Credit Limit", "Interest Rate");
                System.out.println(colors.ANSI_RESET);
                    while (result.next()) {
                        hasResults = true;
                        
                        System.out.printf("%20s %20s %20s %20s %20s\n", 
                        String.format("%05d", result.getInt("card_num")),
                        "$" + result.getString("due_bal"),
                        "$" + result.getString("running_bal"),
                        "$" + result.getString("credit_limit"), 
                        result.getString("interest_rate") + "%");
                    }
                if (!hasResults)
                System.out.println("               No credit cards on file");
                
            /* UNSECURED LOANS */
            hasResults = false;
            query = 
            "SELECT " +
            "unsecured_loan.loan_id, unsecured_loan.amount, " + 
            "unsecured_loan.interest_rate, unsecured_loan.monthly_payment " +
            "FROM unsecured_loan join loan " +
            "ON unsecured_loan.loan_id = loan.loan_id " +
            "WHERE loan.ssn = " + currentCustomer.getSSN();

            result = s.executeQuery(query);
    
                System.out.println(colors.NUMBER + colors.ANSI_UNDER + "Unsecured Loans" + colors.ANSI_RESET);
                System.out.print(colors.ANSI_BOLD  + colors.ANSI_UNDER);
                System.out.printf("%20s %20s %20s %20s %20s \n", "Loan Id", "Amount", "Monthly Payment", "", "Interest Rate");
                System.out.println(colors.ANSI_RESET);
                    while (result.next()) {
                        hasResults = true;
                        
                        System.out.printf("%20s %20s %20s %20s\n", 
                        String.format("%05d", result.getInt("loan_id")),
                        "$" + result.getString("amount"), 
                        "$" + result.getString("monthly_payment"),
                        "",
                        result.getString("interest_rate") + "%"); 
                                
                    }
                if (!hasResults)
                System.out.println("               No unsecured loans on file");

            /* Mortgages */
            hasResults = false;
            query = 
            "SELECT " +
            "mortgage.loan_id, mortgage.amount, " + 
            "mortgage.interest_rate, mortgage.monthly_payment, mortgage.house_id " +
            "FROM mortgage join loan " +
            "ON mortgage.loan_id = loan.loan_id " +
            "WHERE loan.ssn = " + currentCustomer.getSSN();

            result = s.executeQuery(query);
    
                System.out.println(colors.NUMBER + colors.ANSI_UNDER + "Mortgages" + colors.ANSI_RESET);
                System.out.print(colors.ANSI_BOLD  + colors.ANSI_UNDER);
                System.out.printf("%20s %20s %20s %20s %20s\n", "Mortgage Id", "Amount", "Address ID", "Monthly Payment",  "Interest Rate");
                System.out.println(colors.ANSI_RESET);
                    while (result.next()) {
                        hasResults = true;
                        
                        System.out.printf("%20s %20s %20s %20s %20s\n", 
                        String.format("%05d", result.getInt("loan_id")),
                        "$" + result.getString("amount"), 
                        result.getString("house_id"),
                        "$" + result.getString("monthly_payment"),
                        result.getString("interest_rate") + "%"
                        ); 
                                
                    }
                if (!hasResults)
                System.out.println("               No mortgages on file");
                String trash = stdin.nextLine();
    }
    /* retrieves the address of the current customer from the database and puts it into their information*/
    public static void getAddress(Connection con) throws SQLException, IOException{
        Statement s = con.createStatement();
        String query;
        ResultSet result;

            query = 
            "select address.* " + 
            "from customer join lives_at " +
            "on customer.ssn = lives_at.ssn " +
            "join address " +
            "on address.house_id = lives_at.house_id " +
            "where customer.ssn = " + currentCustomer.getSSN();
            result = s.executeQuery(query);
    
            
            if (result.next()) {
                currentCustomer.setHouseId(result.getInt("house_id"));
                currentCustomer.setHouseNum(result.getInt("house_num"));
                currentCustomer.setZip(result.getInt("zip"));
                currentCustomer.setStreetName(result.getString("street_name"));
                currentCustomer.setCity(result.getString("city"));
                currentCustomer.setState(result.getString("state"));
            }
            else {
                currentCustomer.setHouseId(-1); 
            }
    }
    /*prints the customers personal information and address in a pretty way */
    public static void printPersonal() {
        System.out.print(colors.ANSI_BOLD); 
        System.out.printf("%-15s","Name: ");
        System.out.print(colors.ANSI_RESET + colors.TEXT_WHITE); 
        System.out.printf("%-20s", currentCustomer.getName());
        System.out.print(colors.ANSI_BOLD); 
        System.out.println("\t\tAddress");

        System.out.print(colors.ANSI_BOLD); 
        System.out.printf("%-15s","SSN: ");
        System.out.print(colors.ANSI_RESET + colors.TEXT_WHITE); 
        System.out.printf("%-20s", currentCustomer.getSSN());
        System.out.println("\t\t" + currentCustomer.getHouseNum() + " " + currentCustomer.getStreetname());

        System.out.print(colors.ANSI_BOLD); 
        System.out.printf("%-15s","Phone Number: ");
        System.out.print(colors.ANSI_RESET + colors.TEXT_WHITE); 
        System.out.printf("%-20s", currentCustomer.getPhoneNum());
        System.out.println("\t\t" + currentCustomer.getCity() + ", " + currentCustomer.getState());

        System.out.print(colors.ANSI_BOLD); 
        System.out.printf("%-15s","Email: ");
        System.out.print(colors.ANSI_RESET + colors.TEXT_WHITE); 
        System.out.printf("%-20s", currentCustomer.getEmail());
        System.out.println("\t\t" + String.format("%05d", currentCustomer.getZip()) +  "\n");


    }
    /* opens a new account based on user info and generated account number, puts into the database */
    public static void openAccount(Connection con) throws SQLException, IOException {
        Scanner stdin = new Scanner(System.in);
        clear();
        System.out.println(colors.ANSI_BOLD + "  Would you like to open a savings account or a checking account?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Savings\n" +
                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " Checking\n" +
                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
        int input;
        input = readInt();
        
        /* SAVINGS */
        if (input == 1) {
            int account_num;
            double interest_rate;
            double min_balance;

            Statement s = con.createStatement();
            String query;
            ResultSet result;

            query = " select max(account_num) from savings_account";
            result = s.executeQuery(query);
            result.next();
            account_num = 1 + result.getInt("max(account_num)");

            clear();
            System.out.println(colors.ANSI_BOLD + "Savings Account:\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
            System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);                                                           
            System.out.println(String.format("%05d", account_num));
            System.out.print(colors.ANSI_BOLD + "Interest Rate: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            interest_rate = readDouble();
            while (interest_rate == -1) {
                System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.print(colors.ANSI_BOLD + "Interest Rate: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                interest_rate = readDouble();
            }
            System.out.print(colors.ANSI_BOLD + "Minimum Balance: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            min_balance = readDouble();
            while (min_balance == -1) {
                System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.print(colors.ANSI_BOLD + "Minimum Balance: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                min_balance = readDouble();
            }




            query = "insert into account (account_num, account_type) values (" + account_num + ", 'savings')";
            result = s.executeQuery(query);
            query = "insert into savings_account (account_num, interest_rate, balance, min_balance) values (" 
            + account_num + ", " + interest_rate + ", 0," + min_balance + ")"; 
            result = s.executeQuery(query);
            query = "insert into customers_on_account(account_num , ssn) values (" + account_num + ", " + currentCustomer.getSSN() + ")";
            result = s.executeQuery(query);

            clear();
            System.out.println(colors.ANSI_BOLD + "Savings account successfully opened!" + colors.ANSI_RESET + colors.TEXT_WHITE);
            String trash = stdin.nextLine();
        }

        /* CHECKING */
        else if (input == 2) {
                int account_num;
                double interest_rate;
                double min_balance;
    
                Statement s = con.createStatement();
                String query;
                ResultSet result;
    
                query = " select max(account_num) from checking_account";
                result = s.executeQuery(query);
                result.next();
                account_num = 1 + result.getInt("max(account_num)");
    
                clear();
                System.out.println(colors.ANSI_BOLD + "Checking Account:\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);                                                           
                System.out.println(String.format("%05d", account_num));
                System.out.print(colors.ANSI_BOLD + "Interest Rate: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                interest_rate = readDouble();
                while (interest_rate == -1) {
                    System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                    System.out.print(colors.ANSI_BOLD + "Interest Rate: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    interest_rate = readDouble();
                }
    
                query = "insert into account (account_num, account_type) values (" + account_num + ", 'checking')";
                result = s.executeQuery(query);
                query = "insert into checking_account (account_num, interest_rate, balance) values (" 
                + account_num + ", " + interest_rate + ", 0)"; 
                result = s.executeQuery(query);
                query = "insert into customers_on_account(account_num , ssn) values (" + account_num + ", " + currentCustomer.getSSN() + ")";
                result = s.executeQuery(query);
    
                clear();
                System.out.println(colors.ANSI_BOLD + "Checking account successfully opened!" + colors.ANSI_RESET + colors.TEXT_WHITE);
                String trash = stdin.nextLine();

        }

        /* MAIN MENU */
        else if (input == 9) {
            return;
        }

        /* IF NOT 1 OR 2 RECALL THE FUNCTION */
        else {
            openAccount(con);
        }
    }
    /* deposits into a user specified account */
    public static void deposit(Connection con) throws SQLException, IOException {
        clear();
        System.out.println(colors.ANSI_BOLD + "  Would you like to deposit into a savings account or a checking account?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Savings\n" +
                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " Checking\n" +
                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
        int input;
        input = readInt();

        /* SAVINGS */
        if (input == 1) {
            
            clear();
            int account_num = -1;
            System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                account_num = readInt();
            while (account_num == -1) {
                clear();
                System.out.print(colors.TEXT_RED + "Plese enter a valid number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                account_num = readInt();
            }
            Statement s = con.createStatement();
            String query;
            ResultSet result;

            query = 
                "SELECT account.account_num, account.account_type, savings_account.balance, savings_account.min_balance, savings_account.interest_rate " +
                "FROM " +
                "customer join customers_on_account ON customer.SSN = customers_on_account.SSN " +
                "join account ON account.account_num = customers_on_account.account_num " +
                "join savings_account ON account.account_num = savings_account.account_num " +
                "WHERE customer.ssn = " + currentCustomer.getSSN() + 
                " and account.account_type = 'savings'" +
                " and account.account_num = " + account_num;
            result = s.executeQuery(query);

                if (result.next()) {
                    clear();
                    double amount;
                    System.out.print(colors.ANSI_BOLD + "Deposit Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    amount = readDouble();
                    while (amount == -1) {
                        clear();
                        System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.print(colors.ANSI_BOLD + "Deposit Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                        amount = readDouble();
                    }
                    double balance = amount + result.getDouble("balance");
                    int bankID;
                    String date;
                    int d_id;

                    while(true) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + " \nWhich branch are you at?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " 7,   Live Oak         Beaufort  NC  28462\n" +
                                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " 234, Tennesse Avenue  Orange    ME  10393\n");
                        input = readInt();
                        if (input == 1) {
                            bankID = 201;
                            break;
                        }
                        else if (input == 2) {
                            bankID = 202;
                            break;
                        }
                    }
                    date = getDate();
                    query = " select max(d_id) from deposits";
                    result = s.executeQuery(query);
                    result.next();
                    d_id = 1 + result.getInt("max(d_id)");

                    query = 
                    "UPDATE savings_account SET balance = " + balance + " WHERE account_num = " + account_num;
                    result = s.executeQuery(query);

                    query = 
                    "insert into deposits (account_num , amount , bank_id, date_of_deposit, d_id)" +
                     "values (" + account_num + ", " + amount + ", " + bankID + ", '" + date + "', " + d_id + ")";
                    result = s.executeQuery(query);
                }
                else {
                    boolean chosen = false;
                    while (!chosen) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + "  Sorry, we dont have any savings accounts with that account number on your file.\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Retry Deposit\n" +
                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
                        input = readInt();
                        if (input == 1) {
                            chosen = true;
                            deposit(con);
                            return;
                        }
                        else if (input == 9) {
                            return;
                        }     
                    }
                }
        }
            else if (input == 2) {

                clear();
                int account_num = -1;
                System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                account_num = readInt();
                while (account_num == -1) {
                    clear();
                    System.out.print(colors.TEXT_RED + "Plese enter a valid number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    account_num = readInt();
                }
                Statement s = con.createStatement();
                String query;
                ResultSet result;

                query = 
                "SELECT account.account_num, account.account_type, checking_account.balance " +
                "FROM " +
                "customer join customers_on_account ON customer.SSN = customers_on_account.SSN " +
                "join account ON account.account_num = customers_on_account.account_num " +
                "join checking_account ON account.account_num = checking_account.account_num " +
                "WHERE customer.ssn = " + currentCustomer.getSSN() + 
                " and account.account_type = 'checking'" +
                " and account.account_num = " + account_num;
                result = s.executeQuery(query);
                if (result.next()) {
                    clear();
                    double amount;
                    System.out.print(colors.ANSI_BOLD + "Deposit Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    amount = readDouble();
                    while (amount == -1) {
                        clear();
                        System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.print(colors.ANSI_BOLD + "Deposit Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                        amount = readDouble();
                    }
                    double balance = amount + result.getDouble("balance");
                    int bankID;
                    String date;
                    int d_id;

                    while(true) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + " \nWhich branch are you at?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " 7,   Live Oak         Beaufort  NC  28462\n" +
                                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " 234, Tennesse Avenue  Orange    ME  10393\n");
                        input = readInt();
                        if (input == 1) {
                            bankID = 201;
                            break;
                        }
                        else if (input == 2) {
                            bankID = 202;
                            break;
                        }
                    }
                    date = getDate();
                    query = " select max(d_id) from deposits";
                    result = s.executeQuery(query);
                    result.next();
                    d_id = 1 + result.getInt("max(d_id)");

                    query = 
                    "UPDATE checking_account SET balance = " + balance + " WHERE account_num = " + account_num;
                    result = s.executeQuery(query);

                    query = 
                    "insert into deposits (account_num , amount , bank_id, date_of_deposit, d_id)" +
                     "values (" + account_num + ", " + amount + ", " + bankID + ", '" + date + "', " + d_id + ")";
                    result = s.executeQuery(query);
                    System.out.println("here");
                }
                else {
                    boolean chosen = false;
                    while (!chosen) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + "  Sorry, we dont have any checking accounts with that account number on your file.\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Retry Deposit\n" +
                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
                        input = readInt();
                        if (input == 1) {
                            chosen = true;
                            deposit(con);
                            return;
                        }
                        else if (input == 9) {
                            return;
                        }     
                    }
                }
            }
            else if (input == 9) {
                return;
            }
            else {
                deposit(con);
                return;
            }   
            return;     
    }
    /* gets the date from the user in a pretty inefficient way, they can also enter dates that dont exist ie: feb 31 */
    public static String getDate(){

        int day;
        int month;
        int year;
            clear();
            while(true) {
                System.out.print(colors.ANSI_BOLD + "Enter Day (1-31): " + colors.ANSI_RESET + colors.TEXT_WHITE);
                day = readInt();
                if (day < 32 && day > 0)
                break;
            }
            while(true) {
                System.out.print(colors.ANSI_BOLD + "Enter Month (1-12): " + colors.ANSI_RESET + colors.TEXT_WHITE);
                month = readInt();
                if (month < 13 && month > 0)
                break;
            }
            System.out.print(colors.ANSI_BOLD + "Enter Year: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            year = readInt();
            return (day + "/" + month + "/" + year);

    }
    /* withdraws from a user specified account at a bank or with a card at an atm */
    public static void withdraw(Connection con) throws SQLException, IOException {
        Scanner stdin = new Scanner(System.in);
        
        clear();
        System.out.println(colors.ANSI_BOLD + " Are you making a withdrawl from an ATM or Bank?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " ATM\n" +
                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " Bank\n" +
                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
        int input;
        input = readInt();

        Statement s = con.createStatement();
        String query;
        ResultSet result;

            /* At ATM */
            /* using debit card to withdraw from a checking account */
        if (input == 1) { 
                int ATM_ID;
                int card_num;
                String date;
                double amount;
                int w_id;   
                int account_num;
                
                query = " select max(w_id) from withdrawls";
                    result = s.executeQuery(query);
                    result.next();
                    w_id = 1 + result.getInt("max(w_id)");

                while(true) {
                    clear();
                    System.out.println(colors.ANSI_BOLD + " \nWhich ATM are you at?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                    System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " 7,   Live Oak         Beaufort   NC  28462\n" +
                                       colors.NUMBER + "  2:" + colors.ANSI_RESET + " 234, Tennesse Avenue  Orange     ME  10393\n" +
                                       colors.NUMBER + "  3:" + colors.ANSI_RESET + " 2,   ATM Street       ATMville   MA  93791\n" +
                                       colors.NUMBER + "  4:" + colors.ANSI_RESET + " 18,  Street Road      Cityville  CA  09876\n" +
                                       colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");

                    input = readInt();
                    if (input == 1) {
                        ATM_ID = 101;
                        break;
                    }
                    else if (input == 2) {
                        ATM_ID = 102;
                        break;
                    }
                    if (input == 3) {
                        ATM_ID = 001;
                        break;
                    }
                    else if (input == 4) {
                        ATM_ID = 002;
                        break;
                    }
                    else if (input == 9) {
                        return;
                }
            }

            clear();
            card_num = -1;
            System.out.print(colors.ANSI_BOLD + "Card Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                card_num = readInt();
            while (card_num == -1) {
                clear();
                System.out.print(colors.TEXT_RED + "Plese enter a valid number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.print(colors.ANSI_BOLD + "\nCard Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                card_num = readInt();
            }

            query = 
                "select debit_card.card_num, debit_card.account_num, checking_account.balance, checking_account.account_num" + 
                " from debit_card join customer" + 
                " on debit_card.ssn = customer.ssn join checking_account on debit_card.account_num = checking_account.account_num" + 
                " where customer.ssn = " + currentCustomer.getSSN() +
                " and debit_card.card_num = " + card_num; 

                result = s.executeQuery(query);

                if (result.next()) {
                    account_num = result.getInt("account_num");
                    clear();
                    System.out.print(colors.ANSI_BOLD + "Withdraw Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    amount = readDouble();
                    while (amount == -1) {
                        clear();
                        System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.print(colors.ANSI_BOLD + "Withdraw Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                        amount = readDouble();
                    }
                    double balance = result.getDouble("balance") - amount;
                    if (balance < 0) {
                        System.out.print(colors.TEXT_RED + "Insufficient Funds\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        String trash = stdin.nextLine();
                        return;
                    }
                    date = getDate();

                    /* HAVE ALL VARIABLES NOW DO INSERT STATEMENTS */
                    query = 
                    "UPDATE checking_account SET balance = " + balance + " WHERE account_num = " + account_num;
                    result = s.executeQuery(query);

                    query = 
                    "insert into withdrawls (account_num, card_num, amount, ATM_ID, date_of_withdrawl, w_id)" +
                    "values (" + account_num + ", " + card_num + ", " + amount + ", " + ATM_ID + ", '" + date + "', " + w_id + ")";
                    result = s.executeQuery(query);
                    return;

                }

                else {
                    boolean chosen = false;
                    while (!chosen) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + "  Sorry, we dont have any debit cards with that card number on your file.\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Retry withdraw\n" +
                                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
                        input = readInt();
                        if (input == 1) {
                            chosen = true;
                            withdraw(con);
                            return;
                        }
                        else if (input == 9) {
                            return;
                        }     
                    }
                }

        } /* end at atm */
            /* if at bank */
            else if (input == 2) {
                int w_id; 
                int bank_ID;
                String date;
                double amount;  
                int account_num;
                
                query = " select max(w_id) from withdrawls";
                    result = s.executeQuery(query);
                    result.next();
                    w_id = 1 + result.getInt("max(w_id)");

                    while(true) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + " \nWhich branch are you at?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " 7,   Live Oak         Beaufort  NC  28462\n" +
                                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " 234, Tennesse Avenue  Orange    ME  10393\n");
                        input = readInt();
                        if (input == 1) {
                            bank_ID = 201;
                            break;
                        }
                        else if (input == 2) {
                            bank_ID = 202;
                            break;
                        }
                    }

                    while(true) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + " \nAre you withdrawing from a checking or savings account?\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Savings\n" +
                                           colors.NUMBER + "  2:" + colors.ANSI_RESET + " Checking\n");
                        input = readInt();
   /*SAVINGS ACCOUNTS */if (input == 1) {


                            clear();
                            account_num = -1;
                            System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                                account_num = readInt();
                            while (account_num == -1) {
                                clear();
                                System.out.print(colors.TEXT_RED + "Plese enter a valid number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                                System.out.print(colors.ANSI_BOLD + "\nAccount Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                                account_num = readInt();
                            }
                                
                query = 
                "SELECT savings_account.* " +
                "FROM savings_account join account on savings_account.account_num = account.account_num " +
                "join customers_on_account on customers_on_account.account_num = account.account_num " +
                "join customer on customer.ssn = customers_on_account.ssn " +
                "WHERE savings_account.account_num = " + account_num + " and customer.ssn = " + currentCustomer.getSSN();

                result = s.executeQuery(query);

                if (result.next()) {
                    clear();
                    System.out.print(colors.ANSI_BOLD + "Withdraw Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    amount = readDouble();
                    while (amount == -1) {
                        clear();
                        System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.print(colors.ANSI_BOLD + "Withdraw Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                        amount = readDouble();
                    }
                    double balance = result.getDouble("balance") - amount;
                    if (balance < result.getDouble("min_balance")) {
                        System.out.print(colors.TEXT_RED + "Insufficent funds, your minimum balance is: " + result.getDouble("min_balance") + "\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        String trash = stdin.nextLine();
                        return;
                    }
                    date = getDate();

                    /* HAVE ALL VARIABLES NOW DO INSERT STATEMENTS */
                    query = 
                    "UPDATE savings_account SET balance = " + balance + " WHERE account_num = " + account_num;
                    result = s.executeQuery(query);

                    query = 
                    "insert into withdrawls (account_num, amount, bank_id, date_of_withdrawl, w_id)" +
                    "values (" + account_num + ", " + amount + ", " + bank_ID + ", '" + date + "', " + w_id + ")";
                    result = s.executeQuery(query);
                    return;

                }

                else {
                    boolean chosen = false;
                    while (!chosen) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + "  Sorry, we dont have any savings accounts with that account number on your file.\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Retry withdraw\n" +
                                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
                        input = readInt();
                        if (input == 1) {
                            chosen = true;
                            withdraw(con);
                            return;
                        }
                        else if (input == 9) {
                            return;
                        }     
                        }
                        }
                    /* end savings */ }
                        
                    
  /*CHECKING ACCOUNTS */else if (input == 2) {

                            clear();
                            account_num = -1;
                            System.out.print(colors.ANSI_BOLD + "Account Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                                account_num = readInt();
                            while (account_num == -1) {
                                clear();
                                System.out.print(colors.TEXT_RED + "Plese enter a valid number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                                System.out.print(colors.ANSI_BOLD + "\nAccount Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                                account_num = readInt();
                            }
                                
                query = 
                "SELECT checking_account.* " +
                "FROM checking_account join account on checking_account.account_num = account.account_num " +
                "join customers_on_account on customers_on_account.account_num = account.account_num " +
                "join customer on customer.ssn = customers_on_account.ssn " +
                "WHERE checking_account.account_num = " + account_num + " and customer.ssn = " + currentCustomer.getSSN();

                result = s.executeQuery(query);

                if (result.next()) {
                    clear();
                    System.out.print(colors.ANSI_BOLD + "Withdraw Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    amount = readDouble();
                    while (amount == -1) {
                        clear();
                        System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.print(colors.ANSI_BOLD + "Withdraw Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                        amount = readDouble();
                    }
                    double balance = result.getDouble("balance") - amount;
                    if (balance < 0) {
                        System.out.print(colors.TEXT_RED + "Insufficient Funds\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        String trash = stdin.nextLine();
                        return;
                    }
                    date = getDate();

                    /* HAVE ALL VARIABLES NOW DO INSERT STATEMENTS */
                    query = 
                    "UPDATE checking_account SET balance = " + balance + " WHERE account_num = " + account_num;
                    result = s.executeQuery(query);

                    query = 
                    "insert into withdrawls (account_num, amount, bank_id, date_of_withdrawl, w_id)" +
                    "values (" + account_num + ", " + amount + ", " + bank_ID + ", '" + date + "', " + w_id + ")";
                    result = s.executeQuery(query);
                    return;

                }

                else {
                    boolean chosen = false;
                    while (!chosen) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + "  Sorry, we dont have any checking accounts with that account number on your file.\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Retry withdraw\n" +
                                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
                        input = readInt();
                        if (input == 1) {
                            chosen = true;
                            withdraw(con);
                            return;
                        }
                        else if (input == 9) {
                            return;
                        }     
                        }
                        }
/* end checking */                      }
 /* END chose checking or savings  */      }    
/*END AT BANK */}
            else if (input == 9) {
                return;
            }
            else {
                withdraw(con);
            }

    }
    /* uses a specified card to make a credit payment */
    public static void creditPayment(Connection con) throws SQLException, IOException {
        Scanner stdin = new Scanner(System.in);
        int vendor_ID = getVendor();
        int card_num;
        double amount;
        String date;
        int input;
        clear ();
        System.out.print(colors.ANSI_BOLD + "Card Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
        card_num = readInt();
        while (card_num == -1) {
            clear();
            System.out.print(colors.TEXT_RED + "Plese enter a valid number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            System.out.print(colors.ANSI_BOLD + "\nCard Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            card_num = readInt();
        }

        Statement s = con.createStatement();
        String query;
        ResultSet result;

        query = "SELECT * from credit_card where card_num = " + card_num + " and ssn = " + currentCustomer.getSSN();
        result = s.executeQuery(query);

        if (result.next()) {
            clear();
            System.out.print(colors.ANSI_BOLD + "Transaction Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
            amount = readDouble();
            while (amount == -1) {
                clear();
                System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.print(colors.ANSI_BOLD + "Transaction Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                amount = readDouble();
            }
            double running_bal = result.getDouble("running_bal") + amount;
            if (running_bal > result.getDouble("credit_limit")) {
                System.out.print(colors.TEXT_RED + "This transaction will send you over your credit limit of $" + result.getDouble("credit_limit")  + "\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                String trash = stdin.nextLine();
                return;
                }
            date = getDate();

            /* HAVE VALUES NOW DO QUERYS */
            query = "UPDATE credit_card SET running_bal = " + running_bal + " WHERE card_num = " + card_num;
            result = s.executeQuery(query);

            query = 
            "insert into credit_transactions (card_num, vendor_id, amount, date_of_transaction) " +
            "values (" + card_num + ", " + vendor_ID + ", " + amount + ", '"+  date + "')";
            result = s.executeQuery(query);
            
            System.out.print(colors.ANSI_BOLD + "Successfully paid $" + amount + " with your credit card" + colors.ANSI_RESET + colors.TEXT_WHITE);
            String trash = stdin.nextLine();



        }

        else {
            boolean chosen = false;
            while (!chosen) {
                clear();
                System.out.println(colors.ANSI_BOLD + "  Sorry, we dont have any credit cards with that card number on your file.\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Retry transaction\n" +
                                   colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
                input = readInt();
                if (input == 1) {
                    chosen = true;
                    creditPayment(con);
                    return;
                    }
                else if (input == 9) {
                    return;
                    }     
                }
            }




        



    }
    /* uses a specified debit card to make a payment to a vendor */
    public static void debitPayment(Connection con) throws SQLException, IOException {
        Scanner stdin = new Scanner(System.in);

        int input;

        Statement s = con.createStatement();
        String query;
        ResultSet result;

        int vendor_ID = getVendor();
                int card_num;
                String date;
                double amount;  
                int account_num;

                while(true) {
                   

            clear();
            card_num = -1;
            System.out.print(colors.ANSI_BOLD + "Card Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                card_num = readInt();
            while (card_num == -1) {
                clear();
                System.out.print(colors.TEXT_RED + "Plese enter a valid number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                System.out.print(colors.ANSI_BOLD + "\nCard Number: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                card_num = readInt();
            }

            query = 
                "select debit_card.card_num, debit_card.account_num, checking_account.balance, checking_account.account_num" + 
                " from debit_card join customer" + 
                " on debit_card.ssn = customer.ssn join checking_account on debit_card.account_num = checking_account.account_num" + 
                " where customer.ssn = " + currentCustomer.getSSN() +
                " and debit_card.card_num = " + card_num; 

                result = s.executeQuery(query);

                if (result.next()) {
                    account_num = result.getInt("account_num");
                    clear();
                    System.out.print(colors.ANSI_BOLD + "Transaction Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                    amount = readDouble();
                    while (amount == -1) {
                        clear();
                        System.out.print(colors.TEXT_RED + "Please enter a positive number\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.print(colors.ANSI_BOLD + "Transaction Amount: " + colors.ANSI_RESET + colors.TEXT_WHITE);
                        amount = readDouble();
                    }
                    double balance = result.getDouble("balance") - amount;
                    if (balance < 0) {
                        System.out.print(colors.TEXT_RED + "Insufficient Funds\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        String trash = stdin.nextLine();
                        return;
                    }
                    date = getDate();

                    /* HAVE ALL VARIABLES NOW DO INSERT STATEMENTS */
                    query = 
                    "UPDATE checking_account SET balance = " + balance + " WHERE account_num = " + account_num;
                    result = s.executeQuery(query);

                    query = 
                         "insert into debit_transactions (card_num, vendor_id, amount, date_of_transaction) " +
                         "values (" + card_num + ", " + vendor_ID + ", " + amount + ", '" +  date + "')";
                        result = s.executeQuery(query);

                        System.out.print(colors.ANSI_BOLD + "Successfully paid $" + amount + " with your debit card" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        String trash = stdin.nextLine();
            
                    return;

                }

                else {
                    boolean chosen = false;
                    while (!chosen) {
                        clear();
                        System.out.println(colors.ANSI_BOLD + "  Sorry, we dont have any debit cards with that card number on your file.\n" + colors.ANSI_RESET + colors.TEXT_WHITE);
                        System.out.println(colors.NUMBER + "  1:" + colors.ANSI_RESET + " Retry withdraw\n" +
                                           colors.NUMBER + "  9:" + colors.ANSI_RESET + " Main Menu\n");
                        input = readInt();
                        if (input == 1) {
                            chosen = true;
                            withdraw(con);
                            return;
                        }
                        else if (input == 9) {
                            return;
                        }     
                    }
                }

        }


    }

    public static int getVendor() { 
        int input = -1;
        while (!(input > 0 && input <= 7)) {
        clear();
        System.out.println(colors.ANSI_BOLD + "Select a vendor: " + colors.ANSI_RESET + colors.TEXT_WHITE + "\n");
        System.out.println(colors.NUMBER + "  1: " + colors.ANSI_RESET + " McDonalds");
        System.out.println(colors.NUMBER + "  2: " + colors.ANSI_RESET + " Target");
        System.out.println(colors.NUMBER + "  3: " + colors.ANSI_RESET + " Spotify");
        System.out.println(colors.NUMBER + "  4: " + colors.ANSI_RESET + " Supreme");
        System.out.println(colors.NUMBER + "  5: " + colors.ANSI_RESET + " Five Guys");
        System.out.println(colors.NUMBER + "  6: " + colors.ANSI_RESET + " Rite Aid");
        System.out.println(colors.NUMBER + "  7: " + colors.ANSI_RESET + " Neff");
        input = readInt();
        }
        return input;
    }


    




}