

public class colors {
    
	public static final String ANSI_RESET  	= "\u001B[0m";
	public static final String ANSI_BOLD 	= "\u001b[1m";
	public static final String ANSI_UNDER 	= "\u001b[4m";
	public static final String ANSI_CLEAR 	= "\033[H\033[2J";
	public static final String TEXT_BLACK  = "\u001B[30m";
	public static final String TEXT_RED    = "\u001B[31m";
	public static final String TEXT_GREEN  = "\u001B[32m";
	public static final String TEXT_YELLOW = "\u001B[33m";
	public static final String TEXT_BLUE   = "\u001B[34m";
	public static final String TEXT_PURPLE = "\u001B[35m";
	public static final String TEXT_CYAN   = "\u001B[36m";
	public static final String TEXT_WHITE  = "\u001B[37m";
	public static final String TEXT_BRIGHT_BLACK  = "\u001B[90m";
	public static final String TEXT_BRIGHT_RED    = "\u001B[91m";
	public static final String TEXT_BRIGHT_GREEN  = "\u001B[92m";
	public static final String TEXT_BRIGHT_YELLOW = "\u001B[93m";
	public static final String TEXT_BRIGHT_BLUE   = "\u001B[94m";
	public static final String TEXT_BRIGHT_PURPLE = "\u001B[95m";
	public static final String TEXT_BRIGHT_CYAN   = "\u001B[96m";
	public static final String TEXT_BRIGHT_WHITE  = "\u001B[97m";
	public static final String BG_BLACK 	= "\u001b[40m";
	public static final String BG_RED 		= "\u001b[41m";
	public static final String BG_GREEN 	= "\u001b[42m";
	public static final String BG_YELLOW 	= "\u001b[43m";
	public static final String BG_BLUE 		= "\u001b[44m";
	public static final String BG_PURPLE 	= "\u001b[45m";
	public static final String BG_CYAN 		= "\u001b[46m";
	public static final String BG_WHITE 	= "\u001b[47m";

	public static final String NUMBER = (colors.ANSI_BOLD + colors.TEXT_YELLOW);
}
