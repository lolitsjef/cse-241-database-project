import java.io.*;
import java.sql.*;
import java.util.*;



public class mainfile {
    static Scanner stdin = new Scanner(System.in);
    public static void main (String[] arg) 
    throws SQLException, IOException, java.lang.ClassNotFoundException {
      
        funcs.welcome();
        boolean connected = false;
        int input;
        while (!connected) {
            funcs.welcome();
            input = funcs.readInt();
            if (input == 1) {
                connected = true; 
            }
            else if (input  == 2){
                funcs.help();
                String trash = stdin.nextLine();
            }
            else if (input  == 9){
              System.exit(0);
            }
      }
        Connection con = funcs.connect();
        funcs.login(con);
        while(true) {
          try {
            funcs.mainMenu();
            input = funcs.readInt();
            switch(input) {
                case 1:
                  funcs.viewInfo(con);
                  break;
                case 2:
                  funcs.openAccount(con);
                  break;
                case 3:
                  funcs.deposit(con);
                  break;
                case 4:
                  funcs.withdraw(con);
                  break;
                case 5:
                  funcs.creditPayment(con);
                  break;
                case 6:
                  funcs.debitPayment(con);
                  break;
                case 7:
                  funcs.help();
                  String trash = stdin.nextLine();
                  break;
                case 8:
                  funcs.login(con);
                  break;
                case 9:
                  System.exit(0);
                  break;
                default:
              }
            } catch (Exception e) { };
        }
    }
   
}