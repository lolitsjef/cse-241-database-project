--------------------------------------------------------
--  File created - Sunday-April-26-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ACCOUNT
--------------------------------------------------------

  CREATE TABLE "JFV222"."ACCOUNT" 
   (	"ACCOUNT_NUM" NUMBER, 
	"ACCOUNT_TYPE" VARCHAR2(8)
   )
--------------------------------------------------------
--  DDL for Table ADDRESS
--------------------------------------------------------

  CREATE TABLE "JFV222"."ADDRESS" 
   (	"HOUSE_ID" NUMBER, 
	"STREET_NAME" VARCHAR2(20), 
	"HOUSE_NUM" NUMBER, 
	"CITY" VARCHAR2(20), 
	"STATE" VARCHAR2(20), 
	"ZIP" NUMBER
   )
--------------------------------------------------------
--  DDL for Table ATM
--------------------------------------------------------

  CREATE TABLE "JFV222"."ATM" 
   (	"CURRENT_BALANCE" NUMBER, 
	"ATM_ID" NUMBER, 
	"HOUSE_ID" NUMBER
   )
--------------------------------------------------------
--  DDL for Table BANK
--------------------------------------------------------

  CREATE TABLE "JFV222"."BANK" 
   (	"BANK_ID" NUMBER, 
	"CASH_BALANCE" NUMBER, 
	"ATM_ID" NUMBER, 
	"HOUSE_ID" NUMBER
   )
--------------------------------------------------------
--  DDL for Table CHECKING_ACCOUNT
--------------------------------------------------------

  CREATE TABLE "JFV222"."CHECKING_ACCOUNT" 
   (	"ACCOUNT_NUM" NUMBER, 
	"INTEREST_RATE" NUMBER, 
	"BALANCE" NUMBER
   )
--------------------------------------------------------
--  DDL for Table CREDIT_CARD
--------------------------------------------------------

  CREATE TABLE "JFV222"."CREDIT_CARD" 
   (	"CARD_NUM" NUMBER, 
	"SSN" NUMBER, 
	"INTEREST_RATE" NUMBER, 
	"CREDIT_LIMIT" NUMBER, 
	"RUNNING_BAL" NUMBER, 
	"DUE_BAL" NUMBER
   )
--------------------------------------------------------
--  DDL for Table CREDIT_TRANSACTIONS
--------------------------------------------------------

  CREATE TABLE "JFV222"."CREDIT_TRANSACTIONS" 
   (	"VENDOR_ID" NUMBER, 
	"CARD_NUM" NUMBER, 
	"AMOUNT" NUMBER, 
	"DATE_OF_TRANSACTION" VARCHAR2(20)
   )
--------------------------------------------------------
--  DDL for Table CUSTOMER
--------------------------------------------------------

  CREATE TABLE "JFV222"."CUSTOMER" 
   (	"NAME" VARCHAR2(30), 
	"SSN" NUMBER, 
	"PHONE_NUM" VARCHAR2(20), 
	"EMAIL" VARCHAR2(40)
   )
--------------------------------------------------------
--  DDL for Table CUSTOMERS_ON_ACCOUNT
--------------------------------------------------------

  CREATE TABLE "JFV222"."CUSTOMERS_ON_ACCOUNT" 
   (	"ACCOUNT_NUM" NUMBER, 
	"SSN" NUMBER
   )
--------------------------------------------------------
--  DDL for Table DEBIT_CARD
--------------------------------------------------------

  CREATE TABLE "JFV222"."DEBIT_CARD" 
   (	"ACCOUNT_NUM" NUMBER, 
	"CARD_NUM" NUMBER, 
	"SSN" NUMBER
   )
--------------------------------------------------------
--  DDL for Table DEBIT_TRANSACTIONS
--------------------------------------------------------

  CREATE TABLE "JFV222"."DEBIT_TRANSACTIONS" 
   (	"VENDOR_ID" NUMBER, 
	"CARD_NUM" NUMBER, 
	"AMOUNT" NUMBER, 
	"DATE_OF_TRANSACTION" VARCHAR2(20)
   )
--------------------------------------------------------
--  DDL for Table DEPOSITS
--------------------------------------------------------

  CREATE TABLE "JFV222"."DEPOSITS" 
   (	"ACCOUNT_NUM" NUMBER, 
	"BANK_ID" NUMBER, 
	"AMOUNT" NUMBER, 
	"DATE_OF_DEPOSIT" VARCHAR2(20), 
	"D_ID" NUMBER
   )
--------------------------------------------------------
--  DDL for Table LIVES_AT
--------------------------------------------------------

  CREATE TABLE "JFV222"."LIVES_AT" 
   (	"SSN" NUMBER, 
	"HOUSE_ID" NUMBER
   )
--------------------------------------------------------
--  DDL for Table LOAN
--------------------------------------------------------

  CREATE TABLE "JFV222"."LOAN" 
   (	"SSN" NUMBER, 
	"LOAN_ID" NUMBER, 
	"LOAN_TYPE" VARCHAR2(20)
   )
--------------------------------------------------------
--  DDL for Table MORTGAGE
--------------------------------------------------------

  CREATE TABLE "JFV222"."MORTGAGE" 
   (	"LOAN_ID" NUMBER, 
	"AMOUNT" NUMBER, 
	"INTEREST_RATE" VARCHAR2(20), 
	"MONTHLY_PAYMENT" VARCHAR2(20), 
	"HOUSE_ID" NUMBER
   )
--------------------------------------------------------
--  DDL for Table SAVINGS_ACCOUNT
--------------------------------------------------------

  CREATE TABLE "JFV222"."SAVINGS_ACCOUNT" 
   (	"ACCOUNT_NUM" NUMBER, 
	"INTEREST_RATE" NUMBER, 
	"BALANCE" NUMBER, 
	"MIN_BALANCE" NUMBER
   )
--------------------------------------------------------
--  DDL for Table UNSECURED_LOAN
--------------------------------------------------------

  CREATE TABLE "JFV222"."UNSECURED_LOAN" 
   (	"LOAN_ID" NUMBER, 
	"AMOUNT" NUMBER, 
	"INTEREST_RATE" NUMBER, 
	"MONTHLY_PAYMENT" VARCHAR2(20)
   )
--------------------------------------------------------
--  DDL for Table VENDORS
--------------------------------------------------------

  CREATE TABLE "JFV222"."VENDORS" 
   (	"VENDOR_ID" NUMBER, 
	"VENDOR_NAME" VARCHAR2(20)
   )
--------------------------------------------------------
--  DDL for Table WITHDRAWLS
--------------------------------------------------------

  CREATE TABLE "JFV222"."WITHDRAWLS" 
   (	"ACCOUNT_NUM" NUMBER, 
	"AMOUNT" NUMBER, 
	"BANK_ID" NUMBER, 
	"ATM_ID" NUMBER, 
	"DATE_OF_WITHDRAWL" VARCHAR2(20), 
	"W_ID" NUMBER, 
	"CARD_NUM" NUMBER
   )
REM INSERTING into JFV222.ACCOUNT
SET DEFINE OFF;
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (1,'savings');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (2,'savings');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (3,'savings');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (4,'savings');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (5,'savings');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (6,'savings');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (10001,'checking');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (10002,'checking');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (10003,'checking');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (10004,'checking');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (10005,'checking');
Insert into JFV222.ACCOUNT (ACCOUNT_NUM,ACCOUNT_TYPE) values (10006,'checking');
REM INSERTING into JFV222.ADDRESS
SET DEFINE OFF;
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (1,'Willow Ln',2,'Stardew','NC',4523);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (2,'Summit Dr',5,'Hingham','MA',2043);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (3,'Baker St',23,'Shloopie','NY',38295);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (4,'Upper Sayre Pk Rd',97,'Bethlehem','PA',18015);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (5,'Klimbo Ln',24,'Moonlight','ME',58294);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (6,'Unlucky St',13,'Mirror','RI',4523);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (7,'Supreme St',-1,'Tea','AZ',13337);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (8,'Four St',3,'Five Town','NY',12345);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (9,'Bank St',5,'Money','CA',4523);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (10,'Cash Row',55,'Big Bucks','NC',48933);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (11,'Banko Banko Road',2,'Crypto','NY',39820);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (13,'ATM Street',2,'ATMville','MA',93791);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (14,'Street Road',18,'Cityville','CA',9876);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (15,'Live Oak',7,'Beaufort','NC',28462);
Insert into JFV222.ADDRESS (HOUSE_ID,STREET_NAME,HOUSE_NUM,CITY,STATE,ZIP) values (16,'Tennesse Avenue',234,'Orange','ME',10393);
REM INSERTING into JFV222.ATM
SET DEFINE OFF;
Insert into JFV222.ATM (CURRENT_BALANCE,ATM_ID,HOUSE_ID) values (5000,1,13);
Insert into JFV222.ATM (CURRENT_BALANCE,ATM_ID,HOUSE_ID) values (1000,2,14);
Insert into JFV222.ATM (CURRENT_BALANCE,ATM_ID,HOUSE_ID) values (10000,101,15);
Insert into JFV222.ATM (CURRENT_BALANCE,ATM_ID,HOUSE_ID) values (15000,102,16);
REM INSERTING into JFV222.BANK
SET DEFINE OFF;
Insert into JFV222.BANK (BANK_ID,CASH_BALANCE,ATM_ID,HOUSE_ID) values (201,30000,101,15);
Insert into JFV222.BANK (BANK_ID,CASH_BALANCE,ATM_ID,HOUSE_ID) values (202,45000,102,16);
REM INSERTING into JFV222.CHECKING_ACCOUNT
SET DEFINE OFF;
Insert into JFV222.CHECKING_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE) values (10001,0.01,10000);
Insert into JFV222.CHECKING_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE) values (10002,0.02,250);
Insert into JFV222.CHECKING_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE) values (10003,0.03,3333);
Insert into JFV222.CHECKING_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE) values (10004,0.02,768);
Insert into JFV222.CHECKING_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE) values (10005,0.01,35);
Insert into JFV222.CHECKING_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE) values (10006,0.01,6969);
REM INSERTING into JFV222.CREDIT_CARD
SET DEFINE OFF;
Insert into JFV222.CREDIT_CARD (CARD_NUM,SSN,INTEREST_RATE,CREDIT_LIMIT,RUNNING_BAL,DUE_BAL) values (20001,222222222,2,2000,0,0);
Insert into JFV222.CREDIT_CARD (CARD_NUM,SSN,INTEREST_RATE,CREDIT_LIMIT,RUNNING_BAL,DUE_BAL) values (20002,444444444,1,1500,500,0);
Insert into JFV222.CREDIT_CARD (CARD_NUM,SSN,INTEREST_RATE,CREDIT_LIMIT,RUNNING_BAL,DUE_BAL) values (20003,777777777,3,5000,4000,300);
REM INSERTING into JFV222.CREDIT_TRANSACTIONS
SET DEFINE OFF;
Insert into JFV222.CREDIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (3,20001,50,'3/30/2020');
Insert into JFV222.CREDIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (4,20002,33,'2/3/2020');
Insert into JFV222.CREDIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (4,20001,200,'2/2/2020');
Insert into JFV222.CREDIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (3,20001,50,'1/28/2020');
Insert into JFV222.CREDIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (2,20003,65,'1/20/2020');
Insert into JFV222.CREDIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (1,20003,10,'1/5/2020');
REM INSERTING into JFV222.CUSTOMER
SET DEFINE OFF;
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Jeff Buskirk',111111111,'3392361629','jfv222@lehigh.edu');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Kirk Cobb',222222222,'7814571123','kirkland@gmail.com');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Harrison Skappa',333333333,'4209116969','harrison@gmail.edu');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Kyra Hollenbach',444444444,'1894726491','kyra@lehigh.edu');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Blake Wilkey',555555555,'2903483929','blake@wilkey.gov');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Luke Lenny',666666666,'3782047810','lucas@lennington.edu');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Griffin Forest',777777777,'102947392','forest@lehigh.edu');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Eric Picard',888888888,'239104847','picard@looper.edu');
Insert into JFV222.CUSTOMER (NAME,SSN,PHONE_NUM,EMAIL) values ('Eric Meskin',999999999,'4213498204','coolguyeric@lehigh.edu');
REM INSERTING into JFV222.CUSTOMERS_ON_ACCOUNT
SET DEFINE OFF;
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (1,111111111);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (1,444444444);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (2,111111111);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (3,444444444);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (4,999999999);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (4,888888888);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (5,222222222);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (6,333333333);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10001,555555555);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10002,222222222);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10002,777777777);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10003,111111111);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10004,666666666);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10005,333333333);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10005,777777777);
Insert into JFV222.CUSTOMERS_ON_ACCOUNT (ACCOUNT_NUM,SSN) values (10006,444444444);
REM INSERTING into JFV222.DEBIT_CARD
SET DEFINE OFF;
Insert into JFV222.DEBIT_CARD (ACCOUNT_NUM,CARD_NUM,SSN) values (10001,30001,555555555);
Insert into JFV222.DEBIT_CARD (ACCOUNT_NUM,CARD_NUM,SSN) values (10005,30002,333333333);
Insert into JFV222.DEBIT_CARD (ACCOUNT_NUM,CARD_NUM,SSN) values (10002,30003,222222222);
Insert into JFV222.DEBIT_CARD (ACCOUNT_NUM,CARD_NUM,SSN) values (10002,30004,777777777);
REM INSERTING into JFV222.DEBIT_TRANSACTIONS
SET DEFINE OFF;
Insert into JFV222.DEBIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (7,30001,30,'3/24/2020');
Insert into JFV222.DEBIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (1,30002,5,'3/14/2020');
Insert into JFV222.DEBIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (4,30003,21,'2/26/2020');
Insert into JFV222.DEBIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (7,30003,11,'2/4/2020');
Insert into JFV222.DEBIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (3,30002,8,'1/25/2020');
Insert into JFV222.DEBIT_TRANSACTIONS (VENDOR_ID,CARD_NUM,AMOUNT,DATE_OF_TRANSACTION) values (6,30004,25,'1/7/2020');
REM INSERTING into JFV222.DEPOSITS
SET DEFINE OFF;
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (1,202,25,'3/21/2020',8);
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (3,201,380,'3/14/2020',7);
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (10001,202,65,'3/10/2020',6);
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (10002,201,50,'3/8/2020',5);
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (4,202,450,'2/21/2020',4);
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (6,201,1800,'2/4/2020',3);
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (10004,201,93,'2/2/2020',2);
Insert into JFV222.DEPOSITS (ACCOUNT_NUM,BANK_ID,AMOUNT,DATE_OF_DEPOSIT,D_ID) values (2,202,800,'1/21/2020',1);
REM INSERTING into JFV222.LIVES_AT
SET DEFINE OFF;
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (111111111,1);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (444444444,1);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (222222222,2);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (333333333,3);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (555555555,4);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (666666666,5);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (777777777,6);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (888888888,7);
Insert into JFV222.LIVES_AT (SSN,HOUSE_ID) values (999999999,8);
REM INSERTING into JFV222.LOAN
SET DEFINE OFF;
Insert into JFV222.LOAN (SSN,LOAN_ID,LOAN_TYPE) values (666666666,101,'unsecured');
Insert into JFV222.LOAN (SSN,LOAN_ID,LOAN_TYPE) values (999999999,102,'unsecured');
Insert into JFV222.LOAN (SSN,LOAN_ID,LOAN_TYPE) values (555555555,103,'unsecured');
Insert into JFV222.LOAN (SSN,LOAN_ID,LOAN_TYPE) values (333333333,201,'mortgage');
Insert into JFV222.LOAN (SSN,LOAN_ID,LOAN_TYPE) values (222222222,202,'mortgage');
REM INSERTING into JFV222.MORTGAGE
SET DEFINE OFF;
Insert into JFV222.MORTGAGE (LOAN_ID,AMOUNT,INTEREST_RATE,MONTHLY_PAYMENT,HOUSE_ID) values (201,500000,'3','3000',3);
Insert into JFV222.MORTGAGE (LOAN_ID,AMOUNT,INTEREST_RATE,MONTHLY_PAYMENT,HOUSE_ID) values (202,200000,'5','15000',4);
REM INSERTING into JFV222.SAVINGS_ACCOUNT
SET DEFINE OFF;
Insert into JFV222.SAVINGS_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE,MIN_BALANCE) values (1,0.02,10053,3000);
Insert into JFV222.SAVINGS_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE,MIN_BALANCE) values (2,0.02,12345,4000);
Insert into JFV222.SAVINGS_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE,MIN_BALANCE) values (3,0.02,5000,3000);
Insert into JFV222.SAVINGS_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE,MIN_BALANCE) values (4,0.03,25000,6000);
Insert into JFV222.SAVINGS_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE,MIN_BALANCE) values (5,0.02,12333,2000);
Insert into JFV222.SAVINGS_ACCOUNT (ACCOUNT_NUM,INTEREST_RATE,BALANCE,MIN_BALANCE) values (6,0.03,2400,1000);
REM INSERTING into JFV222.UNSECURED_LOAN
SET DEFINE OFF;
Insert into JFV222.UNSECURED_LOAN (LOAN_ID,AMOUNT,INTEREST_RATE,MONTHLY_PAYMENT) values (101,10000,5,'500');
Insert into JFV222.UNSECURED_LOAN (LOAN_ID,AMOUNT,INTEREST_RATE,MONTHLY_PAYMENT) values (102,25000,7,'1200');
Insert into JFV222.UNSECURED_LOAN (LOAN_ID,AMOUNT,INTEREST_RATE,MONTHLY_PAYMENT) values (103,4000,3,'200');
REM INSERTING into JFV222.VENDORS
SET DEFINE OFF;
Insert into JFV222.VENDORS (VENDOR_ID,VENDOR_NAME) values (1,'McDonalds');
Insert into JFV222.VENDORS (VENDOR_ID,VENDOR_NAME) values (2,'Target');
Insert into JFV222.VENDORS (VENDOR_ID,VENDOR_NAME) values (3,'Spotify');
Insert into JFV222.VENDORS (VENDOR_ID,VENDOR_NAME) values (4,'Supreme');
Insert into JFV222.VENDORS (VENDOR_ID,VENDOR_NAME) values (5,'Five Guys');
Insert into JFV222.VENDORS (VENDOR_ID,VENDOR_NAME) values (6,'Rite Aid');
Insert into JFV222.VENDORS (VENDOR_ID,VENDOR_NAME) values (7,'Neff');
REM INSERTING into JFV222.WITHDRAWLS
SET DEFINE OFF;
Insert into JFV222.WITHDRAWLS (ACCOUNT_NUM,AMOUNT,BANK_ID,ATM_ID,DATE_OF_WITHDRAWL,W_ID,CARD_NUM) values (10005,5,201,null,'3/6/2020',10,null);
Insert into JFV222.WITHDRAWLS (ACCOUNT_NUM,AMOUNT,BANK_ID,ATM_ID,DATE_OF_WITHDRAWL,W_ID,CARD_NUM) values (2,305,201,null,'2/26/2020',9,null);
Insert into JFV222.WITHDRAWLS (ACCOUNT_NUM,AMOUNT,BANK_ID,ATM_ID,DATE_OF_WITHDRAWL,W_ID,CARD_NUM) values (10006,35,202,null,'2/4/2020',6,null);
Insert into JFV222.WITHDRAWLS (ACCOUNT_NUM,AMOUNT,BANK_ID,ATM_ID,DATE_OF_WITHDRAWL,W_ID,CARD_NUM) values (6,300,202,null,'1/18/2020',4,null);
Insert into JFV222.WITHDRAWLS (ACCOUNT_NUM,AMOUNT,BANK_ID,ATM_ID,DATE_OF_WITHDRAWL,W_ID,CARD_NUM) values (10004,30,201,null,'1/8/2020',3,null);
Insert into JFV222.WITHDRAWLS (ACCOUNT_NUM,AMOUNT,BANK_ID,ATM_ID,DATE_OF_WITHDRAWL,W_ID,CARD_NUM) values (3,50,202,null,'1/8/2020',2,null);
Insert into JFV222.WITHDRAWLS (ACCOUNT_NUM,AMOUNT,BANK_ID,ATM_ID,DATE_OF_WITHDRAWL,W_ID,CARD_NUM) values (10001,15,201,null,'1/2/2020',1,null);
--------------------------------------------------------
--  DDL for Index ACCOUNT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."ACCOUNT_PK" ON "JFV222"."ACCOUNT" ("ACCOUNT_NUM")
--------------------------------------------------------
--  DDL for Index ADDRESS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."ADDRESS_PK" ON "JFV222"."ADDRESS" ("HOUSE_ID")
--------------------------------------------------------
--  DDL for Index ATM_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."ATM_PK" ON "JFV222"."ATM" ("ATM_ID")
--------------------------------------------------------
--  DDL for Index BANK_PK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."BANK_PK1" ON "JFV222"."BANK" ("BANK_ID")
--------------------------------------------------------
--  DDL for Index CHECKING_ACCOUNT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."CHECKING_ACCOUNT_PK" ON "JFV222"."CHECKING_ACCOUNT" ("ACCOUNT_NUM")
--------------------------------------------------------
--  DDL for Index CREDIT_CARD_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."CREDIT_CARD_PK" ON "JFV222"."CREDIT_CARD" ("CARD_NUM")
--------------------------------------------------------
--  DDL for Index CUSTOMER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."CUSTOMER_PK" ON "JFV222"."CUSTOMER" ("SSN")
--------------------------------------------------------
--  DDL for Index DEBIT_CARD_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."DEBIT_CARD_PK" ON "JFV222"."DEBIT_CARD" ("CARD_NUM")
--------------------------------------------------------
--  DDL for Index DEPOSITS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."DEPOSITS_PK" ON "JFV222"."DEPOSITS" ("D_ID")
--------------------------------------------------------
--  DDL for Index LOAN_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."LOAN_PK" ON "JFV222"."LOAN" ("LOAN_ID")
--------------------------------------------------------
--  DDL for Index MORTGAGE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."MORTGAGE_PK" ON "JFV222"."MORTGAGE" ("LOAN_ID")
--------------------------------------------------------
--  DDL for Index SAVINGS_ACCOUNT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."SAVINGS_ACCOUNT_PK" ON "JFV222"."SAVINGS_ACCOUNT" ("ACCOUNT_NUM")
--------------------------------------------------------
--  DDL for Index UNSECURED_LOAN_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."UNSECURED_LOAN_PK" ON "JFV222"."UNSECURED_LOAN" ("LOAN_ID")
--------------------------------------------------------
--  DDL for Index VENDORS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."VENDORS_PK" ON "JFV222"."VENDORS" ("VENDOR_ID")
--------------------------------------------------------
--  DDL for Index WITHDRAWLS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JFV222"."WITHDRAWLS_PK" ON "JFV222"."WITHDRAWLS" ("W_ID")
--------------------------------------------------------
--  Constraints for Table ACCOUNT
--------------------------------------------------------

  ALTER TABLE "JFV222"."ACCOUNT" MODIFY ("ACCOUNT_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ACCOUNT" MODIFY ("ACCOUNT_TYPE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ACCOUNT" ADD CONSTRAINT "ACCOUNT_PK" PRIMARY KEY ("ACCOUNT_NUM")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table ADDRESS
--------------------------------------------------------

  ALTER TABLE "JFV222"."ADDRESS" MODIFY ("HOUSE_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ADDRESS" MODIFY ("STREET_NAME" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ADDRESS" MODIFY ("HOUSE_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ADDRESS" MODIFY ("CITY" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ADDRESS" MODIFY ("STATE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ADDRESS" MODIFY ("ZIP" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ADDRESS" ADD CONSTRAINT "ADDRESS_PK" PRIMARY KEY ("HOUSE_ID")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table ATM
--------------------------------------------------------

  ALTER TABLE "JFV222"."ATM" ADD CONSTRAINT "ATM_PK" PRIMARY KEY ("ATM_ID")
  USING INDEX  ENABLE
  ALTER TABLE "JFV222"."ATM" MODIFY ("CURRENT_BALANCE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ATM" MODIFY ("ATM_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."ATM" MODIFY ("HOUSE_ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table BANK
--------------------------------------------------------

  ALTER TABLE "JFV222"."BANK" MODIFY ("BANK_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."BANK" MODIFY ("HOUSE_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."BANK" MODIFY ("ATM_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."BANK" ADD CONSTRAINT "BANK_PK" PRIMARY KEY ("BANK_ID")
  USING INDEX (CREATE UNIQUE INDEX "JFV222"."BANK_PK1" ON "JFV222"."BANK" ("BANK_ID") 
  )  ENABLE
  ALTER TABLE "JFV222"."BANK" MODIFY ("CASH_BALANCE" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table CHECKING_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "JFV222"."CHECKING_ACCOUNT" ADD CONSTRAINT "CHECKING_ACCOUNT_PK" PRIMARY KEY ("ACCOUNT_NUM")
  USING INDEX  ENABLE
  ALTER TABLE "JFV222"."CHECKING_ACCOUNT" MODIFY ("ACCOUNT_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CHECKING_ACCOUNT" MODIFY ("INTEREST_RATE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CHECKING_ACCOUNT" MODIFY ("BALANCE" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table CREDIT_CARD
--------------------------------------------------------

  ALTER TABLE "JFV222"."CREDIT_CARD" MODIFY ("CARD_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_CARD" MODIFY ("SSN" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_CARD" MODIFY ("INTEREST_RATE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_CARD" MODIFY ("RUNNING_BAL" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_CARD" MODIFY ("DUE_BAL" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_CARD" ADD CONSTRAINT "CREDIT_CARD_PK" PRIMARY KEY ("CARD_NUM")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table CREDIT_TRANSACTIONS
--------------------------------------------------------

  ALTER TABLE "JFV222"."CREDIT_TRANSACTIONS" MODIFY ("AMOUNT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_TRANSACTIONS" MODIFY ("DATE_OF_TRANSACTION" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_TRANSACTIONS" MODIFY ("VENDOR_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CREDIT_TRANSACTIONS" MODIFY ("CARD_NUM" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table CUSTOMER
--------------------------------------------------------

  ALTER TABLE "JFV222"."CUSTOMER" MODIFY ("NAME" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CUSTOMER" MODIFY ("SSN" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CUSTOMER" MODIFY ("PHONE_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CUSTOMER" MODIFY ("EMAIL" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CUSTOMER" ADD CONSTRAINT "CUSTOMER_PK" PRIMARY KEY ("SSN")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table CUSTOMERS_ON_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "JFV222"."CUSTOMERS_ON_ACCOUNT" MODIFY ("SSN" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."CUSTOMERS_ON_ACCOUNT" MODIFY ("ACCOUNT_NUM" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table DEBIT_CARD
--------------------------------------------------------

  ALTER TABLE "JFV222"."DEBIT_CARD" MODIFY ("ACCOUNT_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEBIT_CARD" MODIFY ("CARD_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEBIT_CARD" ADD CONSTRAINT "DEBIT_CARD_PK" PRIMARY KEY ("CARD_NUM")
  USING INDEX  ENABLE
  ALTER TABLE "JFV222"."DEBIT_CARD" MODIFY ("SSN" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table DEBIT_TRANSACTIONS
--------------------------------------------------------

  ALTER TABLE "JFV222"."DEBIT_TRANSACTIONS" MODIFY ("AMOUNT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEBIT_TRANSACTIONS" MODIFY ("DATE_OF_TRANSACTION" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEBIT_TRANSACTIONS" MODIFY ("VENDOR_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEBIT_TRANSACTIONS" MODIFY ("CARD_NUM" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table DEPOSITS
--------------------------------------------------------

  ALTER TABLE "JFV222"."DEPOSITS" MODIFY ("ACCOUNT_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEPOSITS" MODIFY ("BANK_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEPOSITS" MODIFY ("AMOUNT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEPOSITS" MODIFY ("DATE_OF_DEPOSIT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEPOSITS" MODIFY ("D_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."DEPOSITS" ADD CONSTRAINT "DEPOSITS_PK" PRIMARY KEY ("D_ID")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table LIVES_AT
--------------------------------------------------------

  ALTER TABLE "JFV222"."LIVES_AT" MODIFY ("SSN" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."LIVES_AT" MODIFY ("HOUSE_ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table LOAN
--------------------------------------------------------

  ALTER TABLE "JFV222"."LOAN" MODIFY ("SSN" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."LOAN" MODIFY ("LOAN_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."LOAN" MODIFY ("LOAN_TYPE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."LOAN" ADD CONSTRAINT "LOAN_PK" PRIMARY KEY ("LOAN_ID")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table MORTGAGE
--------------------------------------------------------

  ALTER TABLE "JFV222"."MORTGAGE" MODIFY ("LOAN_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."MORTGAGE" MODIFY ("AMOUNT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."MORTGAGE" MODIFY ("INTEREST_RATE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."MORTGAGE" MODIFY ("MONTHLY_PAYMENT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."MORTGAGE" ADD CONSTRAINT "MORTGAGE_PK" PRIMARY KEY ("LOAN_ID")
  USING INDEX  ENABLE
  ALTER TABLE "JFV222"."MORTGAGE" MODIFY ("HOUSE_ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table SAVINGS_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "JFV222"."SAVINGS_ACCOUNT" ADD CONSTRAINT "SAVINGS_ACCOUNT_PK" PRIMARY KEY ("ACCOUNT_NUM")
  USING INDEX  ENABLE
  ALTER TABLE "JFV222"."SAVINGS_ACCOUNT" MODIFY ("ACCOUNT_NUM" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."SAVINGS_ACCOUNT" MODIFY ("INTEREST_RATE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."SAVINGS_ACCOUNT" MODIFY ("BALANCE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."SAVINGS_ACCOUNT" MODIFY ("MIN_BALANCE" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table UNSECURED_LOAN
--------------------------------------------------------

  ALTER TABLE "JFV222"."UNSECURED_LOAN" MODIFY ("LOAN_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."UNSECURED_LOAN" MODIFY ("AMOUNT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."UNSECURED_LOAN" MODIFY ("INTEREST_RATE" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."UNSECURED_LOAN" MODIFY ("MONTHLY_PAYMENT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."UNSECURED_LOAN" ADD CONSTRAINT "UNSECURED_LOAN_PK" PRIMARY KEY ("LOAN_ID")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table VENDORS
--------------------------------------------------------

  ALTER TABLE "JFV222"."VENDORS" MODIFY ("VENDOR_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."VENDORS" MODIFY ("VENDOR_NAME" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."VENDORS" ADD CONSTRAINT "VENDORS_PK" PRIMARY KEY ("VENDOR_ID")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Constraints for Table WITHDRAWLS
--------------------------------------------------------

  ALTER TABLE "JFV222"."WITHDRAWLS" MODIFY ("AMOUNT" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."WITHDRAWLS" MODIFY ("DATE_OF_WITHDRAWL" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."WITHDRAWLS" MODIFY ("W_ID" NOT NULL ENABLE)
  ALTER TABLE "JFV222"."WITHDRAWLS" ADD CONSTRAINT "WITHDRAWLS_PK" PRIMARY KEY ("W_ID")
  USING INDEX  ENABLE
--------------------------------------------------------
--  Ref Constraints for Table ATM
--------------------------------------------------------

  ALTER TABLE "JFV222"."ATM" ADD CONSTRAINT "HASADDRESS2" FOREIGN KEY ("HOUSE_ID")
	  REFERENCES "JFV222"."ADDRESS" ("HOUSE_ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table BANK
--------------------------------------------------------

  ALTER TABLE "JFV222"."BANK" ADD CONSTRAINT "HASADDRESS" FOREIGN KEY ("HOUSE_ID")
	  REFERENCES "JFV222"."ADDRESS" ("HOUSE_ID") ENABLE
  ALTER TABLE "JFV222"."BANK" ADD CONSTRAINT "HASATM" FOREIGN KEY ("ATM_ID")
	  REFERENCES "JFV222"."ATM" ("ATM_ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table CHECKING_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "JFV222"."CHECKING_ACCOUNT" ADD CONSTRAINT "ISACCOUNT2" FOREIGN KEY ("ACCOUNT_NUM")
	  REFERENCES "JFV222"."ACCOUNT" ("ACCOUNT_NUM") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table CREDIT_CARD
--------------------------------------------------------

  ALTER TABLE "JFV222"."CREDIT_CARD" ADD CONSTRAINT "HASCARD" FOREIGN KEY ("SSN")
	  REFERENCES "JFV222"."CUSTOMER" ("SSN") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table CREDIT_TRANSACTIONS
--------------------------------------------------------

  ALTER TABLE "JFV222"."CREDIT_TRANSACTIONS" ADD CONSTRAINT "CREDITCARDPAYMENT" FOREIGN KEY ("CARD_NUM")
	  REFERENCES "JFV222"."CREDIT_CARD" ("CARD_NUM") ENABLE
  ALTER TABLE "JFV222"."CREDIT_TRANSACTIONS" ADD CONSTRAINT "VENDORLINK2" FOREIGN KEY ("VENDOR_ID")
	  REFERENCES "JFV222"."VENDORS" ("VENDOR_ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table CUSTOMERS_ON_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "JFV222"."CUSTOMERS_ON_ACCOUNT" ADD CONSTRAINT "CUSTOMERONACCOUNT" FOREIGN KEY ("SSN")
	  REFERENCES "JFV222"."CUSTOMER" ("SSN") ENABLE
  ALTER TABLE "JFV222"."CUSTOMERS_ON_ACCOUNT" ADD CONSTRAINT "ACCOUNTOFINTEREST" FOREIGN KEY ("ACCOUNT_NUM")
	  REFERENCES "JFV222"."ACCOUNT" ("ACCOUNT_NUM") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table DEBIT_CARD
--------------------------------------------------------

  ALTER TABLE "JFV222"."DEBIT_CARD" ADD CONSTRAINT "LINKEDTOACCOUNT" FOREIGN KEY ("ACCOUNT_NUM")
	  REFERENCES "JFV222"."CHECKING_ACCOUNT" ("ACCOUNT_NUM") ENABLE
  ALTER TABLE "JFV222"."DEBIT_CARD" ADD CONSTRAINT "CUSTOMERONDEBITCARD" FOREIGN KEY ("SSN")
	  REFERENCES "JFV222"."CUSTOMER" ("SSN") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table DEBIT_TRANSACTIONS
--------------------------------------------------------

  ALTER TABLE "JFV222"."DEBIT_TRANSACTIONS" ADD CONSTRAINT "DEBITCARDPAYMENT" FOREIGN KEY ("CARD_NUM")
	  REFERENCES "JFV222"."DEBIT_CARD" ("CARD_NUM") ENABLE
  ALTER TABLE "JFV222"."DEBIT_TRANSACTIONS" ADD CONSTRAINT "VENDORLINK" FOREIGN KEY ("VENDOR_ID")
	  REFERENCES "JFV222"."VENDORS" ("VENDOR_ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table DEPOSITS
--------------------------------------------------------

  ALTER TABLE "JFV222"."DEPOSITS" ADD CONSTRAINT "ATBANK" FOREIGN KEY ("BANK_ID")
	  REFERENCES "JFV222"."BANK" ("BANK_ID") ENABLE
  ALTER TABLE "JFV222"."DEPOSITS" ADD CONSTRAINT "INTOACCOUNT" FOREIGN KEY ("ACCOUNT_NUM")
	  REFERENCES "JFV222"."ACCOUNT" ("ACCOUNT_NUM") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table LIVES_AT
--------------------------------------------------------

  ALTER TABLE "JFV222"."LIVES_AT" ADD CONSTRAINT "ADDRESSOFPERSON" FOREIGN KEY ("HOUSE_ID")
	  REFERENCES "JFV222"."ADDRESS" ("HOUSE_ID") ENABLE
  ALTER TABLE "JFV222"."LIVES_AT" ADD CONSTRAINT "PERSONOFINTEREST" FOREIGN KEY ("SSN")
	  REFERENCES "JFV222"."CUSTOMER" ("SSN") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table LOAN
--------------------------------------------------------

  ALTER TABLE "JFV222"."LOAN" ADD CONSTRAINT "HASLOAN" FOREIGN KEY ("SSN")
	  REFERENCES "JFV222"."CUSTOMER" ("SSN") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table MORTGAGE
--------------------------------------------------------

  ALTER TABLE "JFV222"."MORTGAGE" ADD CONSTRAINT "ISLOAN2" FOREIGN KEY ("LOAN_ID")
	  REFERENCES "JFV222"."LOAN" ("LOAN_ID") ENABLE
  ALTER TABLE "JFV222"."MORTGAGE" ADD CONSTRAINT "MORTGAGEADDRESS" FOREIGN KEY ("HOUSE_ID")
	  REFERENCES "JFV222"."ADDRESS" ("HOUSE_ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table SAVINGS_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "JFV222"."SAVINGS_ACCOUNT" ADD CONSTRAINT "ISACCOUNT" FOREIGN KEY ("ACCOUNT_NUM")
	  REFERENCES "JFV222"."ACCOUNT" ("ACCOUNT_NUM") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table UNSECURED_LOAN
--------------------------------------------------------

  ALTER TABLE "JFV222"."UNSECURED_LOAN" ADD CONSTRAINT "ISLOAN" FOREIGN KEY ("LOAN_ID")
	  REFERENCES "JFV222"."LOAN" ("LOAN_ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table WITHDRAWLS
--------------------------------------------------------

  ALTER TABLE "JFV222"."WITHDRAWLS" ADD CONSTRAINT "CARDUSED" FOREIGN KEY ("CARD_NUM")
	  REFERENCES "JFV222"."DEBIT_CARD" ("CARD_NUM") ENABLE
  ALTER TABLE "JFV222"."WITHDRAWLS" ADD CONSTRAINT "FROMACCOUNT" FOREIGN KEY ("ACCOUNT_NUM")
	  REFERENCES "JFV222"."ACCOUNT" ("ACCOUNT_NUM") ENABLE
  ALTER TABLE "JFV222"."WITHDRAWLS" ADD CONSTRAINT "ATBANK2" FOREIGN KEY ("BANK_ID")
	  REFERENCES "JFV222"."BANK" ("BANK_ID") ENABLE
  ALTER TABLE "JFV222"."WITHDRAWLS" ADD CONSTRAINT "ATATM" FOREIGN KEY ("ATM_ID")
	  REFERENCES "JFV222"."ATM" ("ATM_ID") ENABLE
