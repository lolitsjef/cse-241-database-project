all:
	clear
	javac jfv222/*.java
	mv jfv222/*.class .
	jar cfmv jfv222.jar Manifest.txt *.class
	mv *.class jfv222
run:
	java -jar jfv222.jar
clean:
	rm jfv222.jar
	rm jfv222/*.class
	clear



